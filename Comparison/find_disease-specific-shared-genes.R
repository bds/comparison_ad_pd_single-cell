#compute disease specific, disease shared and disease different genes

library(openxlsx)


library(dplyr)


source("disease_specific_genes_function.R")


cell="all_celltypes" # or astrocyte, neuron, microglial



###########Differential Genes Analyses###############

#Get differentially expressed genes for all cell types or specific cell types
degs_AD=read.xlsx(paste0("../AD/differential_expression_ad_vs_hc_",cell,"_logFC.xlsx"), rowNames = T)#

degs_PD=read.xlsx(paste0("../PD/differential_expression_pd_vs_hc_",cell,"_logFC.xlsx"), rowNames = T)


#See DEGS (Differentially expressed genes) which are significant
dim(degs_AD[degs_AD$p_val_adj < 0.05,])#4552 for AD

dim(degs_PD[degs_PD$p_val_adj < 0.05,])#5514 for PD


#Apply the disease spec genes function which will gives back all the DEGs which are specific, shared and shared but different betwen AD and PD
dfres=disease_spec_genes(degs_AD = degs_AD, degs_PD = degs_PD)

#Get different DEGs (Shared without same direction)
different=dfres[dfres$DEG.type=="disease-different",]

dim(different)#149 for all cell types

#Create new colun which is the absolute difference between the log FC of AD and PD
different$Diff_logFC=abs(different$AD.avg..logFC + different$PD.avg..logFC)

#write in a file the Top 10 different genes (The ones with the most important differences between AD and PD) used to do the table in the article
write.csv(different %>% arrange(desc(Diff_logFC))  %>% slice(1:10) , paste0("Top_Ten_different_",cell,"_genes.csv"))



#get the DEGs which are AD specific
AD_specific=dfres[dfres$DEG.type=="AD-specific",]

dim(AD_specific)#66 all clel types

#write the top Ten used for Table 2 inside article
write.csv(AD_specific %>% 
  arrange(desc(abs(AD_specific$AD.avg..logFC))) %>% slice(1:10),paste0("Top_Ten_AD-specific_",cell,"_genes.csv"))



#get the DEGs which are PD specific
PD_specific=dfres[dfres$DEG.type=="PD-specific",]

dim(PD_specific)#70 all cell types

#write the top Ten used for Table 2 inside article
write.csv(PD_specific %>% 
            arrange(desc(abs(PD_specific$PD.avg..logFC))) %>% slice(1:10),paste0("Top_Ten_PD-specific_",cell,"_genes.csv"))



#get shared DEGS
disease_shared=dfres[dfres$DEG.type=="disease-shared",]

dim(disease_shared)

#get the Top 10
write.csv(disease_shared %>% arrange(desc(abs(PD.avg..logFC)),desc(abs(AD.avg..logFC)) )  %>% slice(1:10) ,paste0("Top_Ten_shared_",cell,"_genes.csv"))


#Create Venn diagramm#

library(nVennR)

myV <- createVennObj(nSets = 5, sNames = c('AD', 'PD','Up','Down','Different'))
myV <- setVennRegion(myV, c('AD'), as.numeric(dim(AD_specific)[1]))
myV <- setVennRegion(myV, c('PD'), as.numeric(dim(PD_specific)[1]))
myV <- setVennRegion(myV,c('AD','PD','Up'), as.numeric(dim(disease_shared[disease_shared$AD.avg..logFC>0,])[1]))
myV <- setVennRegion(myV,c('AD','PD','Down'), as.numeric(dim(disease_shared[disease_shared$AD.avg..logFC<0,])[1]))
myV <- setVennRegion(myV,c('AD','PD','Different'), as.numeric(dim(different)[1]))
myV <- plotVenn(nVennObj = myV, outFile=paste0("diag_",cell,".svg"), opacity = 0.1, labelRegions = F, fontScale = 1.5, setColors = c('orange', 'purple', 'green', 'red', 'blue'))




##Get the DEGS referenced as Neuroprotective (Table 3)

#Genes referenced as neuroprotective in NeuroproDB
neuropro=read.table("neuropro_v1.txt", sep="\t", header=T, fill=T)


df_neuropo=as.data.frame(neuropro)


intersect(df_neuropo$Gene.Symbol, PD_specific$Gene.symbols)

intersect(df_neuropo$Gene.Symbol, AD_specific$Gene.symbols)

intersect(df_neuropo$Gene.Symbol, disease_shared$Gene.symbols)

intersect(df_neuropo$Gene.Symbol, different$Gene.symbols)






###########Pathway analyses###########


#LOad required librqry
library(org.Hs.eg.db)
library(clusterProfiler)


#get all categories of DEGs
DEGs=c("different","AD_specific","PD_specific","disease_shared")
for (i in 1:4){#for each categories
  DEG=eval(as.name(DEGs[i]))
  de<- c(DEG$Gene.symbols)
  for (el in c("BP", "MF")){#For Biological process and molecular function
    
    yy<- enrichGO(de, 'org.Hs.eg.db', keyType="SYMBOL", ont=el, pvalueCutoff=0.11, qvalueCutoff=0.11)#see if there is an overrepresentation among the categorie of DEGs
    print(paste0("./Top_",el,"_",DEGs[i],"_",cell,".png"))#checking
    if (!all(is.na(yy[1]))){#If there is one or more BP or MF (! NA)
      png(paste0("./Top_",el,"_",DEGs[i],"_",cell,".png"), width=8, height=6, units="in", res=300)#print dotplot for the categorie of DEG, for BP or MF in the concerned cell types
      print(dotplot(yy, showCategory=6, color="qvalue", font.size=16))
      dev.off()#Create png inside your working directory if there is an overepresentation under specified Q value
    }
    }
  
}



