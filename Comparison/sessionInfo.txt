R version 4.1.2 (2021-11-01)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 22.04.3 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.10.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.10.0

locale:
 [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C               LC_TIME=en_GB.UTF-8        LC_COLLATE=en_US.UTF-8    
 [5] LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_US.UTF-8    LC_PAPER=en_GB.UTF-8       LC_NAME=C                 
 [9] LC_ADDRESS=C               LC_TELEPHONE=C             LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       

attached base packages:
[1] stats4    stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] clusterProfiler_4.2.2   org.Hs.eg.db_3.14.0     AnnotationDbi_1.56.2    IRanges_2.28.0         
 [5] S4Vectors_0.32.4        Biobase_2.54.0          BiocGenerics_0.40.0     nVennR_0.2.3           
 [9] lubridate_1.9.2         forcats_1.0.0           stringr_1.5.0           purrr_1.0.2            
[13] readr_2.1.4             tidyr_1.3.0             tibble_3.2.1            tidyverse_2.0.0        
[17] openxlsx_4.2.5.2        dplyr_1.1.3             cluster_2.1.2           ggplot2_3.4.3          
[21] Seurat_4.9.9.9050       SeuratObject_4.9.9.9086 sp_2.0-0               

loaded via a namespace (and not attached):
  [1] utf8_1.2.3                  spatstat.explore_3.2-3      reticulate_1.31             tidyselect_1.2.0           
  [5] RSQLite_2.3.1               htmlwidgets_1.6.2           grid_4.1.2                  BiocParallel_1.28.3        
  [9] Rtsne_0.16                  scatterpie_0.2.1            munsell_0.5.0               codetools_0.2-18           
 [13] ica_1.0-3                   future_1.33.0               miniUI_0.1.1.1              withr_2.5.0                
 [17] spatstat.random_3.1-5       colorspace_2.1-0            GOSemSim_2.20.0             progressr_0.14.0           
 [21] rstudioapi_0.15.0           ROCR_1.0-11                 tensor_1.5                  DOSE_3.20.1                
 [25] listenv_0.9.0               MatrixGenerics_1.6.0        labeling_0.4.3              GenomeInfoDbData_1.2.7     
 [29] polyclip_1.10-4             bit64_4.0.5                 farver_2.1.1                downloader_0.4             
 [33] treeio_1.25.1               parallelly_1.36.0           vctrs_0.6.3                 generics_0.1.3             
 [37] timechange_0.2.0            R6_2.5.1                    GenomeInfoDb_1.30.1         graphlayouts_1.0.0         
 [41] gridGraphics_0.5-1          bitops_1.0-7                spatstat.utils_3.0-3        cachem_1.0.8               
 [45] fgsea_1.20.0                DelayedArray_0.20.0         promises_1.2.1              scales_1.2.1               
 [49] ggraph_2.1.0                enrichplot_1.14.2           gtable_0.3.4                globals_0.16.2             
 [53] goftest_1.2-3               spam_2.9-1                  tidygraph_1.2.3             rlang_1.1.1                
 [57] splines_4.1.2               lazyeval_0.2.2              spatstat.geom_3.2-5         BiocManager_1.30.22        
 [61] reshape2_1.4.4              abind_1.4-5                 httpuv_1.6.11               qvalue_2.26.0              
 [65] tools_4.1.2                 ggplotify_0.1.2             ellipsis_0.3.2              RColorBrewer_1.1-3         
 [69] ggridges_0.5.4              Rcpp_1.0.11                 plyr_1.8.8                  sparseMatrixStats_1.6.0    
 [73] zlibbioc_1.40.0             RCurl_1.98-1.12             deldir_1.0-9                viridis_0.6.4              
 [77] pbapply_1.7-2               cowplot_1.1.1               zoo_1.8-12                  SummarizedExperiment_1.24.0
 [81] ggrepel_0.9.3               magrittr_2.0.3              data.table_1.14.8           RSpectra_0.16-1            
 [85] glmGamPoi_1.6.0             scattermore_1.2             DO.db_2.9                   lmtest_0.9-40              
 [89] RANN_2.6.1                  fitdistrplus_1.1-11         matrixStats_1.0.0           hms_1.1.3                  
 [93] patchwork_1.1.3             mime_0.12                   xtable_1.8-4                mclust_6.0.0               
 [97] fastDummies_1.7.3           gridExtra_2.3               compiler_4.1.2              shadowtext_0.1.2           
[101] KernSmooth_2.23-20          crayon_1.5.2                htmltools_0.5.6             ggfun_0.1.2                
[105] later_1.3.1                 tzdb_0.4.0                  aplot_0.2.0                 DBI_1.1.3                  
[109] tweenr_2.0.2                MASS_7.3-55                 Matrix_1.6-1                cli_3.6.1                  
[113] parallel_4.1.2              dotCall64_1.0-2             igraph_1.5.1                GenomicRanges_1.46.1       
[117] pkgconfig_2.0.3             plotly_4.10.2               spatstat.sparse_3.0-2       ggtree_3.9.0               
[121] XVector_0.34.0              yulab.utils_0.0.9           digest_0.6.33               sctransform_0.3.5          
[125] RcppAnnoy_0.0.21            spatstat.data_3.0-1         Biostrings_2.62.0           leiden_0.4.3               
[129] fastmatch_1.1-4             tidytree_0.4.5              uwot_0.1.16                 DelayedMatrixStats_1.16.0  
[133] shiny_1.7.5                 lifecycle_1.0.3             nlme_3.1-155                jsonlite_1.8.7             
[137] viridisLite_0.4.2           fansi_1.0.4                 pillar_1.9.0                lattice_0.20-45            
[141] KEGGREST_1.34.0             fastmap_1.1.1               httr_1.4.7                  survival_3.2-13            
[145] GO.db_3.14.0                glue_1.6.2                  zip_2.3.0                   png_0.1-8                  
[149] bit_4.0.5                   ggforce_0.4.1               stringi_1.7.12              blob_1.2.4                 
[153] RcppHNSW_0.4.1              memoise_2.0.1               ape_5.7-1                   irlba_2.3.5.1              
[157] future.apply_1.11.0        
