load(file = "sctransv2_ad_annotated.Rdata")
library(Seurat)
library(dplyr)




# Differential analysis - all cell-types combined


Idents(sctransv2) <- "cond"
table(Idents(sctransv2))#CHECK




head(sctransv2@assays$SCT@counts)


ad_vs_hc_markers <- FindMarkers(sctransv2,slot="counts", ident.1 = "AD", ident.2 = "HC", min.pct = 0.10, logfc.threshold = 0, only.pos=False, test.use = "poisson", assay="SCT")

# show top 50 differential genes
head(pd_vs_hc_markers, n = 100)


# write output to Excel ranking table
require('openxlsx')
write.xlsx(ad_vs_hc_markers, file='differential_expression_ad_vs_hc_all_celltypes.xlsx', rowNames=T)

Idents(sctransv2) <- "seurat_clusters"

sctransv2.markers <- FindAllMarkers(sctransv2, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
write.csv(sctransv2.markers %>%
  group_by(cluster) %>%
  slice_max(n = 3, order_by = avg_log2FC),"Top3_marker_cluster.csv")









# Differential analysis - cell-types specific




sctransv2@meta.data$celltype.cond = paste(sctransv2@meta.data$classcellmarker, sctransv2@meta.data$cond, sep="_")


Idents(sctransv2) <- "classcellmarker"

# show cell types and associated cell counts
table(Idents(sctransv2))



#Microglial

dim(sctransv2@assays$SCT@counts[,colnames(sctransv2@assays$SCT@counts) %in% rownames(sctransv2@meta.data[sctransv2@meta.data$classcellmarker=="Microglial cell",])])

ad_vs_hc_microglial_markers <- FindMarkers(sctransv2, slot="counts", ident.1 = "Microglial cell_AD", ident.2 = "Microglial cell_HC", only.pos=False,logfc.theshold=-Inf , test.use = "poisson", assay="SCT")

# show top 50 differential genes
head(ad_vs_hc_microglial_markers, n = 50)

# write output to Excel ranking table
require('openxlsx')
write.xlsx(ad_vs_hc_microglial_markers, file='differential_expression_ad_vs_hc_microglial.xlsx', rowNames=T)



#Neuron


dim(sctransv2@assays$SCT@counts[,colnames(sctransv2@assays$SCT@counts) %in% rownames(sctransv2@meta.data[sctransv2@meta.data$classcellmarker=="Neuron",])])

ad_vs_hc_neuron_markers <- FindMarkers(sctransv2, slot="counts", ident.1 = "Neuron_AD", ident.2 = "Neuron_HC", only.pos=False,logfc.theshold=-Inf , test.use = "poisson", assay="SCT")

# show top 50 differential genes
head(ad_vs_hc_neuron_markers, n = 50)

# write output to Excel ranking table
require('openxlsx')
write.xlsx(ad_vs_hc_neuron_markers, file='differential_expression_ad_vs_hc_neuron.xlsx', rowNames=T)


#Astrocyte


dim(sctransv2@assays$SCT@counts[,colnames(sctransv2@assays$SCT@counts) %in% rownames(sctransv2@meta.data[sctransv2@meta.data$classcellmarker=="Astrocyte",])])

ad_vs_hc_astrocyte_markers <- FindMarkers(sctransv2, slot="counts", ident.1 = "Astrocyte_AD", ident.2 = "Astrocyte_HC", only.pos=False,logfc.theshold=-Inf , test.use = "poisson", assay="SCT")

# show top 50 differential genes
head(ad_vs_hc_astrocyte_markers, n = 50)

# write output to Excel ranking table
require('openxlsx')
write.xlsx(ad_vs_hc_astrocyte_markers, file='differential_expression_ad_vs_hc_astrocyte.xlsx', rowNames=T)





# Violin plots for top 3 differential genes for microglial
if(!require('ggplot2')){

        install.packages('ggplot2')
        require('ggplot2')
}


pdf("violin_plot_differential_expression_pd_vs_hc_all_Microglial_top10.pdf")
for(i in 1:max(3,nrow(pd_vs_hc_microglial_markers))){
        p = VlnPlot(sctransv2, features = rownames(pd_vs_hc_microglial_markers)[i], assay = "SCT", log = TRUE, idents = c("Microglial cell_HC","Microglial cell_PD")) + geom_boxplot(alpha = 0.8)
        print(p)
}
dev.off()



